#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "uart.h"
using namespace std;

struct Student
{
	int roll_number;
	char name[30];
	
        char emailID[100];
	
};

using namespace std;
struct node
{
	node *next;
	node *prev;
         struct	Student * student;
};
void EnterStudent(int roll,char student_name[30],Student *st,char *emailID)
{
	st->roll_number =roll;
	strcpy(st->name,student_name);
	
        strcpy(st->emailID,emailID);
}

void printStudent(struct Student *st)
{
	printf("Student Name : ");
	printf("%s",st->name);
	printf("  <---->  Roll Number : ");
	printf("%d",st->roll_number);
        printf(" <---->  EmailID :%s\n",st->emailID);
	
}

struct Sorted_Doubly_Linked_List
{
        int c;
	node *head ;
	node *tail ; 
	node *p ;
	node *r ;
	node *np ;
	
};
/*Sorted_Doubly_Linked_List::Sorted_Doubly_Linked_List(void)
{
	head = NULL, tail = NULL, p = NULL, r = NULL, np = NULL;
	c=0;
}*/
struct  node *head ;
struct	node *tail ; 
struct	node *p ;
struct	node *r ;
struct	node *np ;
int c;
struct node* Sorted_Doubly_Linked_ListCreate(struct Student *s1)
{
    np = (struct node*)malloc(sizeof(node));
	np->student = s1;
    np->next = NULL;
    np->prev = NULL;
    if (c == 0)
    {
        tail = np;
        head = np;
        p = head;
        p->next = NULL;
        p->prev = NULL;
        c++;

    }
    else
    {
        p = head;
        r = p;
		if (np->student->roll_number< p->student->roll_number)
		{
			np->next = p;
			p->prev = np;
			np->prev = NULL;
			head = np;
			p = head;
			do
			{
				p = p->next;
			}
			while (p->next != NULL);
			tail = p;
		}
		else if (np->student->roll_number > p->student->roll_number)
		{
			while (p != NULL && np->student->roll_number > p->student->roll_number)
			{
				r = p;
				p = p->next;
				if (p == NULL)
				{
					r->next = np;
					np->prev = r;
					np->next = NULL;
					tail = np;
					break;
				}
				else if (np->student->roll_number < p->student->roll_number)
				{ 
					r->next = np;
					np->prev = r;
					np->next = p;
					p->prev = np;
					if (p->next != NULL)
					{
						do
						{
							p = p->next;
						}
						while (p->next !=NULL);
					}
					tail = p;
					break;
				 }
			}
		}
    }
	return np;
}


void Sorted_Doubly_Linked_Listdescending()
{
    struct node *t = tail;
    while (t != NULL)
    {
	printStudent(t->student);
        t = t->prev;
    }
    printf("\n");
}
void Sorted_Doubly_Linked_Listascending()
{
   struct node *t = head;
    while (t != NULL)
    {
        printStudent(t->student);
        t = t->next;
    }
   printf("\n");
}
void Sorted_Doubly_Linked_ListdeleteNode(struct node**, struct node*);

void Sorted_Doubly_Linked_Listdelete_student(int roll)
{
	node *t = head;
	if (head == NULL)
	{
		printf("There are no students\n");
		return;
	}
	while (t->student->roll_number != roll)
    {
        t = t->next;
		if (t==NULL)
			break;
    }
	if(t==NULL)
	{		
		printf("There are no student with this Roll No.\n");
		return;
	}
	if (t->student->roll_number == roll)
	{
		Sorted_Doubly_Linked_ListdeleteNode(&head,t);
		printf("Student Record Deleted\n");
	}
	
}
void Sorted_Doubly_Linked_ListdeleteNode(struct node **head_ref, struct node *del)
{
  /* base case */
  if(*head_ref == NULL || del == NULL)
    return;
 
  /* If node to be deleted is head node */
  if(*head_ref == del)
    *head_ref = del->next;
 
  /* Change next only if node to be deleted is NOT the last node */
  if(del->next != NULL)
    del->next->prev = del->prev;
 
  /* Change prev only if node to be deleted is NOT the first node */
  if(del->prev != NULL)
    del->prev->next = del->next;     
 
  /* Finally, free the memory occupied by del*/
  free(del);
  return;
}
struct BtreeKey
{
	int roll_no;
	node* record;
};
struct BtreeNode
{
   struct BtreeKey *keys;
   struct BtreeNode **ptrs;
   bool leaf;
   int n;
   int mindeg;
};
int mind;
int Predecessor(int,struct BtreeNode*);
int Successor(int,struct BtreeNode*);
void fill(int,struct BtreeNode*);
void Prevborrow(int,struct BtreeNode*);
void Nextborrow(int,struct BtreeNode*);
void Merge(int,struct BtreeNode*);
void LeafDeletion(int,struct BtreeNode*);
void NonLeafDeletion(int,struct BtreeNode*);
int getkey(int,struct BtreeNode*);
void deletion(int,struct BtreeNode*);
int Predecessor(int index,struct BtreeNode *root)
  {
     struct BtreeNode *temp=root->ptrs[index];
     while(!temp->leaf)
       temp=temp->ptrs[temp->n];
     
     return temp->keys[temp->n-1].roll_no;
  }
int Successor(int index,struct BtreeNode *root)
  {
    struct BtreeNode *temp=root->ptrs[index+1];
    while(!temp->leaf)
       temp=temp->ptrs[0];
     
     return temp->keys[0].roll_no;  
  }
  void fill(int index,struct BtreeNode *root)
  {
     if(index!=0 && root->ptrs[index-1]->n>=root->mindeg)
        Prevborrow(index,root);
     else if(index!=root->n && root->ptrs[index+1]->n>=root->mindeg)
        Nextborrow(index,root);

     else
     {
        if(index!=root->n)
          Merge(index,root);
        else
          Merge(index-1,root);
     }
  }
  void Prevborrow(int index,struct BtreeNode *root)
  {
     BtreeNode *fstchild=root->ptrs[index];
     BtreeNode *secchild=root->ptrs[index-1];

     int i,j;
     for(i=fstchild->n-1;i>=0;i--)
         fstchild->keys[i+1]=fstchild->keys[i];

     if(!fstchild->leaf)
     {
         for(i=fstchild->n;i>=0;i--)
             fstchild->ptrs[i+1]=fstchild->ptrs[i];
     }
     fstchild->keys[0]=root->keys[index-1];
     if(!fstchild->leaf)
        fstchild->ptrs[0]=fstchild->ptrs[secchild->n];

      root->keys[index-1]=secchild->keys[secchild->n-1];

      fstchild->n+=1;
      secchild->n-=1;
      
  }
  void Nextborrow(int index,struct BtreeNode *root)
  {
   BtreeNode *fstchild=root->ptrs[index];
   BtreeNode *secchild=root->ptrs[index+1];
   fstchild->keys[fstchild->n]=root->keys[index];
   
   if(!(fstchild->leaf))
     fstchild->ptrs[(fstchild->n)+1]=secchild->ptrs[0];
   
   root->keys[index]=secchild->keys[0];
   int i,j;
   
   for(i=1;i<secchild->n;i++)
     secchild->keys[i-1]=secchild->keys[i];
   
    if(!secchild->leaf)
    {
        for(i=1;i<=secchild->n;i++)
            secchild->ptrs[i-1]=secchild->ptrs[i];
    }

    secchild->n-=1;
    fstchild->n+=1;
    
  }
  void Merge(int index,struct BtreeNode *root)
  {
      BtreeNode *fstchild=root->ptrs[index];
      BtreeNode *secchild=root->ptrs[index+1];
    
      fstchild->keys[root->mindeg-1]=root->keys[index];

      int i,j;
 
      for(i=0;i<secchild->n;i++)
        fstchild->keys[i+root->mindeg]=secchild->keys[i];

      if(!fstchild->leaf)
      {
          for(i=0;i<=secchild->n;i++)
               fstchild->ptrs[i+root->mindeg]=secchild->ptrs[i];
      }

      for(i=index+1;i<root->n;i++)
         root->keys[i-1]=root->keys[i];

      for(i=index+2;i<=root->n;i++)
         root->ptrs[i-1]=root->ptrs[i];

      fstchild->n+=secchild->n;
      fstchild->n+=1;
    
      root->n--;
   
      free(secchild);
  }
void LeafDeletion(int index,struct BtreeNode *root)
  {
      for(int i=index+1;i<root->n;i++)
        root->keys[i-1]=root->keys[i];
      root->n--;
  }
  void NonLeafDeletion(int index,struct BtreeNode *root)
  {
      int d=root->keys[index].roll_no;
      int prev,next;
      if(root->ptrs[index]->n>=root->mindeg)
      {
         prev=Predecessor(index,root);
         root->keys[index].roll_no=prev;
         deletion(prev,root->ptrs[index]);
      }

      else if(root->ptrs[index+1]->n>=root->mindeg)
      {
         next=Successor(index,root);
         root->keys[index].roll_no=next;
         deletion(next,root->ptrs[index+1]);
      }
      
      else
      {
          Merge(index,root);
          deletion(d,root->ptrs[index]);
      } 
  }
int getkey(int d,struct BtreeNode *root)
  {
    int i=0;
    
    while(i<root->n && root->keys[i].roll_no<d)
      i++;
    return i;
  }
void deletion(int d,struct BtreeNode *root)
  {
   int i=getkey(d,root);
   bool f;
   if(i<root->n && root->keys[i].roll_no==d)
   {
       if(root->leaf)
          LeafDeletion(i,root);
       else
          NonLeafDeletion(i,root);
   }
   else
   {
     if(root->leaf)
     {
        printf("No such key is present");
        return;
     }
    f=(i==root->n);
    if(root->ptrs[i]->n<root->mindeg)
     fill(i,root);
    
    if(f&&i>root->n)
      deletion(d,root->ptrs[i-1]);
    else
      deletion(d,root->ptrs[i]);
    }
  }
void splitnode(struct BtreeNode*,int,struct BtreeNode*);
void ins(int k,struct BtreeNode *root,node *rec)
  {
    int j=root->n-1;
    if(root->leaf==1)
    {
     while(j>=0&&k<root->keys[j].roll_no)
     {
        root->keys[j+1]=root->keys[j];
        j--;
     }
    root->keys[j+1].roll_no=k;
    root->keys[j+1].record=rec;
    root->n++;
    }
   else
    {
      while(j>=0&&k<root->keys[j].roll_no)
      j--;
      if(root->ptrs[j+1]->n==2*root->mindeg-1)
      {
           splitnode(root->ptrs[j+1],j+1,root);
           if(root->keys[j+1].roll_no<k)
            j++;
      }
      ins(k,root->ptrs[j+1],rec);
    }
  }
  void splitnode(struct BtreeNode *y,int i,struct BtreeNode *root)
  {
     BtreeNode *z=(BtreeNode*)malloc(sizeof(BtreeNode));
     z->leaf=y->leaf;
     z->mindeg=y->mindeg;
     z->keys=(BtreeKey*)malloc(sizeof(BtreeKey)*(2*root->mindeg-1));
     z->n=0;
     z->ptrs=(struct BtreeNode**)malloc(sizeof(BtreeNode*)*(2*root->mindeg));
     z->n=root->mindeg-1;
	if(y->leaf==0)
     	{
         for(int k=0;k<root->mindeg;k++)
            z->ptrs[k]=y->ptrs[k+root->mindeg];
     	}
      for(int k=0;k<root->mindeg-1;k++)
       z->keys[k]=y->keys[k+root->mindeg];
     
     y->n=root->mindeg-1;
     
    
     for(int k=root->n-1;k>=i;k--)
     root->keys[k+1]=root->keys[k];
     root->keys[i]=y->keys[root->mindeg-1];
     
      for(int j=root->n;j>=i+1;j--)
         root->ptrs[j+1]=root->ptrs[j];
     root->ptrs[i+1]=z;
     root->n=root->n+1;
  }
  void traverse(struct BtreeNode *root)
  {
    int i;
    for(i=0;i<root->n;i++)
    {
        if(root->leaf==0)
        traverse(root->ptrs[i]);
      printf(" %d",root->keys[i].roll_no);
    }
   if(root->leaf==0)
    traverse(root->ptrs[i]);
  }
 struct BtreeNode* search(int data1,struct BtreeNode *root)
  {
    int j=root->n-1;
    while(j>=0&&data1<root->keys[j].roll_no)
    j--;
    if(j>=0&&root->keys[j].roll_no==data1)
     return root;
    if(root->leaf==true)
      return NULL;
    return search(data1,root->ptrs[j+1]);
   }
 
  struct node * search1(int k,struct BtreeNode **root)
  {
    //return (*root == NULL)? 0:search(k,*root);
    if ((search(k,*root)== NULL) || (*root == NULL))
		return NULL;
	else
	{
		BtreeNode* rec_found = search(k,*root);
		for(int i=0;i<rec_found->n;i++)
		{
			if (k==rec_found->keys[i].roll_no)
				return rec_found->keys[i].record;
		}
	
	}
  }
  void traverse1(struct BtreeNode **root)
  {
    if((*root)!=NULL)
    traverse(*root);
  }
  void insertion(int keys,struct BtreeNode **root,node *rec)
  {
  if(*root==NULL)
    {
      *root=(BtreeNode*)malloc(sizeof(BtreeNode));
     (*root)->leaf=1;
     (*root)->mindeg=mind;
     (*root)->keys=(BtreeKey*)malloc(sizeof(BtreeKey)*(2*mind-1));
     (*root)->n=0;
     (*root)->ptrs=(struct BtreeNode**)malloc(sizeof(BtreeNode*)*(2*mind));
        (*root)->keys[0].roll_no=keys;
        (*root)->keys[0].record=rec;
        (*root)->n=1;
    }
    else
    {
        if((*root)->n==2*mind-1)
        {
            BtreeNode *newr=(BtreeNode*)malloc(sizeof(BtreeNode));
     newr->leaf=0;
     newr->mindeg=mind;
     newr->keys=(BtreeKey*)malloc(sizeof(BtreeKey)*(2*mind-1));
     newr->n=0;
     newr->ptrs=(struct BtreeNode**)malloc(sizeof(BtreeNode*)*(2*mind));
            newr->ptrs[0]=*root;
            splitnode(*root,0,newr);
            int j=0;
            if(newr->keys[0].roll_no<keys)
            j++;
            ins(keys,newr->ptrs[j],rec);
            *root=newr;
        }
     else
     ins(keys,*root,rec);
    }
  }
  void deletion1(int keys,struct BtreeNode **root)
  {
     if(!(*root))
     {
       printf("The tree is empty\n");
       return;
     }
     BtreeNode *curr=*root;
     deletion(keys,*root);
     if((*root)->n==0)
     {
        if((*root)->leaf)
           (*root)=NULL;
        else
           (*root)=(*root)->ptrs[0];
     
         free(curr);
     }
   }
        
int main()
{
	//int mind;
	//BTree t;
	//Sorted_Doubly_Linked_List sdll;
	int ch1;
	printf("	     <---------   DataBase System    ------->    \n\n");
	printf("1 ) Load Database from an existing file\n2 ) Create a new Database\n\nEnter Option : ");
	scanf("%d",&ch1);
        int j;
       struct BtreeNode* root=NULL;
	if (ch1 == 1)
	{
		
                 char rder[1000];
                 FILE *fptr;
                 if ((fptr = fopen("data.txt", "r")) == NULL) {
                        printf("Error! opening file");
                    // Program exits if file pointer returns NULL.
                     exit(1);
                        }
                fgets(rder, 512, fptr);
		//int order = stoi(rder);
                 int order=0;
                int i=0;
                while(rder[i]!='\n'&&rder[i]!='\0')
                {
                    order=order*10;
                    order+=rder[i]-'0';
                    i++;
                }
		mind = order;
		char line[1000];
		//t.set_order(order);
                char inputarr[200];
                //printf("Order of the Btree is: %d"<<order<<endl;
		while (fgets(inputarr, 512, fptr) != NULL)		
                       {
		
		//sdll.ascending();
    		char newString[200][200];
    		int ctr=0;
    		j=0;
        //cout<<inputarr<<endl;
    	        for(i=0;i<=strlen(inputarr);i++)
    	        {
        // if space or NULL found, assign NULL into newString[ctr]
        	if(inputarr[i]==' '||inputarr[i]=='\0' ||inputarr[i]=='\n')
        	{
        	    newString[ctr][j]='\0';
        	    ctr++;  //for next word
        	    j=0;    //for next word, init index to 0
        	}
        	else
        	{
        	    newString[ctr][j]=inputarr[i];
        	    j++;
        	}
    	}
           char emailid[100];
           int roll_number=0;
           strcpy(emailid,newString[2]);
    //cout<<grd<<endl;
           i=0;
    //cout<<newString[0]<<endl;
          while(newString[0][i]!='\n'&&newString[0][i]!='\0')
          {
         roll_number=roll_number*10;
         //cout<<roll_number<<" ";
         roll_number+=(newString[0][i]-'0');
         i++;
        // cout<<newString[0][i-1]<<" "<<roll_number<<endl;
          }
    //roll_number=newString[0][0]-'0';
         char name[200];
         strcpy(name,newString[1]);
         printf("(%d , %s , %s )\n",roll_number,name,emailid);
         Student *s=(struct Student*)malloc(sizeof(struct Student));
         EnterStudent(roll_number,name,s,emailid);
         node* n;
         n = Sorted_Doubly_Linked_ListCreate(s);
         insertion(roll_number,&root,n);
    //cout<<"\nDo You Want to Insert Another Record ? (Yes == 1,No == 0) : ";
    
       }
       fclose(fptr);
      //Sorted_Doubly_Linked_Listascending();
	}
	else
	{
		// create you own database
		int order;
		printf("Set the order of B-tree : ");
		scanf("%d",&order);
		mind = order;
		//t.set_order(order);
		int i;
		int choice=1;
                head = NULL, tail = NULL, p = NULL, r = NULL, np = NULL;
                c=0;
                //getchar();
                while(choice)
                {
                  printf("1  AliAsher AliAsher@gmail.com   ( Example Record )\n");
                  printf("Enter the record in the above mentioned format : \n");
                  char inputarr[200];
    //string inputarr;
                  int index = 0;
                  char input;
                while ((getchar()) != '\n');
               // scanf("%[^\n]s",inputarr);
                fgets(inputarr,512,stdin);
   // cin>>inputarr;
               // while ((getchar()) != '\n');
    
                char newString[200][200];
                int ctr=0;
                 j=0;
                for(i=0;i<=strlen(inputarr);i++)
                {
        // if space or NULL found, assign NULL into newString[ctr]
                if(inputarr[i]==' '||inputarr[i]=='\0' ||inputarr[i]=='\n')
                {
                   newString[ctr][j]='\0';
                   ctr++;  //for next word
                   j=0;    //for next word, init index to 0
                }
                else
                {
                 newString[ctr][j]=inputarr[i];
                 j++;
                }
             }
            char email[100];
            int roll_number=0;
            strcpy(email,newString[2]);
    //cout<<grd<<endl;
            i=0;
    //cout<<newString[0]<<endl;
           while(newString[0][i]!='\n'&&newString[0][i]!='\0')
           {
              roll_number=roll_number*10;
         //cout<<roll_number<<" ";
             roll_number+=(newString[0][i]-'0');
              i++;
        // cout<<newString[0][i-1]<<" "<<roll_number<<endl;
           }
    //roll_number=newString[0][0]-'0';
          char name[200];
          strcpy(name,newString[1]);
          printf("(%d , %s , %s )\n",roll_number,name,email);
          Student *s=(struct Student*)malloc(sizeof(struct Student));
          EnterStudent(roll_number,name,s,email);
          node* n;
          n = Sorted_Doubly_Linked_ListCreate(s);
          //insertion(roll_number,&root,n);
         printf("\nDo You Want to Insert Another Record ? (Yes == 1,No == 0) : ");
        //while ((getchar()) != '\n');
        scanf("%d",&choice);
        //while ((getchar()) != '\n');
    //cin>>choice;
      }

     }



       while (true)
	{
		printf("	     <---------   Operations    ------->    \n\n");
		printf("1 ) Create Record \n2 ) Read Record \n3 ) Delete Record\n4 ) Update Record \n5 ) View All Records\n6 ) Records in a Range(Ascending) \n7 ) Records in a Range(Descending)\n8 ) Save to File \n\nEnter Option : ");
		//cin>>ch1;
                scanf("%d",&ch1);
		if (ch1 == 1){
			
			printf("1 AliAsher AliAsher@gmail.com   ( Example Record )\n");
			printf("Enter the record in the above mentioned format : \n");
			  char inputarr[200];
    //string inputarr;
                  int index = 0;
                  char input;
    
                while ((getchar()) != '\n');
               // scanf("%[^\n]s",inputarr);
                fgets(inputarr,512,stdin);
   // cin>>inputarr;
               // while ((getchar()) != '\n');
    
                char newString[200][200];
                int ctr=0;
                 j=0;
                int i;
                for(i=0;i<=strlen(inputarr);i++)
                {
        // if space or NULL found, assign NULL into newString[ctr]
                if(inputarr[i]==' '||inputarr[i]=='\0' ||inputarr[i]=='\n')
                {
                   newString[ctr][j]='\0';
                   ctr++;  //for next word
                   j=0;    //for next word, init index to 0
                }
                else
                {
                 newString[ctr][j]=inputarr[i];
                 j++;
                }
             }
            char email[100];
            int roll_number=0;
            strcpy(email,newString[2]);
    //cout<<grd<<endl;
            i=0;
    //cout<<newString[0]<<endl;
           while(newString[0][i]!='\n'&&newString[0][i]!='\0')
           {
              roll_number=roll_number*10;
         //cout<<roll_number<<" ";
             roll_number+=(newString[0][i]-'0');
              i++;
        // cout<<newString[0][i-1]<<" "<<roll_number<<endl;
           }
    //roll_number=newString[0][0]-'0';
          char name[200];
          strcpy(name,newString[1]);
          printf("(%d , %s , %s )\n",roll_number,name,email);
          Student *s=(struct Student*)malloc(sizeof(struct Student));
          EnterStudent(roll_number,name,s,email);
          node* n;
          n = Sorted_Doubly_Linked_ListCreate(s);
          insertion(roll_number,&root,n);
		}
		else if (ch1 == 5)
		{
			Sorted_Doubly_Linked_Listascending();
		}
		else if (ch1 == 2)
		{
			int r;
			printf("Enter the Roll Number of the student : ");
			scanf("%d",&r);
			(search1(r,&root) != NULL)? printf("\nPresent") :printf("\nNot Present");
			if (search1(r,&root) != NULL)
			{
				node* found_record = search1(r,&root);
                                printStudent(found_record->student);
				//found_record->student->print();
			}
		}
		else if (ch1==3)
		{
			int r;
			printf("Enter the Roll Number of the student : ");
			scanf("%d",&r);
			deletion1(r,&root);
			Sorted_Doubly_Linked_Listdelete_student(r);
		}
		else if (ch1==6)
		{
			int r1;
			printf("Enter Starting RollNo : ");
			scanf("%d",&r1);
			int r2;
			printf("Enter Ending RollNo : ");
			scanf("%d",&r2);
			printf("\n");
			for (int i=r1;i<=r2;i++)
			{
				if (search1(i,&root) != NULL)
				{
					node* found_record = search1(i,&root);
					printStudent(found_record->student);
				}
			}
		}
		else if (ch1==7)
		{
			int r1;
			printf("Enter Starting RollNo : ");
			scanf("%d",&r1);
			int r2;
			printf("Enter Ending RollNo : ");
			scanf("%d",&r2);
			printf("\n");
			for (int i=r2;i>=r1;i--)
			{
				if (search1(i,&root) != NULL)
				{
					node* found_record = search1(i,&root);
					printStudent(found_record->student);
				}
			}



		}
		else if(ch1==4)
		{
			int r;
			printf("Enter the Roll Number of the student : ");
			scanf("%d",&r);
			
			if(search1(r,&root) != NULL){ printf("\nPresent") ;}
                        else{printf("\nNot Present");continue;}
			if (search1(r,&root) != NULL)
			{
				node* found_record = search1(r,&root);
                                printStudent(found_record->student);
				//found_record->student->print();
			}
                        else
                                continue;



			printf("1 AliAsher AliAsher@gmail.com   ( Example Record )\n");
			printf("Enter the record in the above mentioned format : \n");
			char inputarr[200];
			int index = 0;
			char input;
			
                        while ((getchar()) != '\n');
                        fgets(inputarr,512,stdin);
			char newString[200][200];
                int ctr=0;
                 j=0;
                int i;
                for(i=0;i<=strlen(inputarr);i++)
                {
        // if space or NULL found, assign NULL into newString[ctr]
                if(inputarr[i]==' '||inputarr[i]=='\0' ||inputarr[i]=='\n')
                {
                   newString[ctr][j]='\0';
                   ctr++;  //for next word
                   j=0;    //for next word, init index to 0
                }
                else
                {
                 newString[ctr][j]=inputarr[i];
                 j++;
                }
             }
            char email[100];
            int roll_number=0;
            strcpy(email,newString[2]);
    
            i=0;
    
           while(newString[0][i]!='\n'&&newString[0][i]!='\0')
           {
              roll_number=roll_number*10;
         
             roll_number+=(newString[0][i]-'0');
              i++;

           }
          char name[200];
          strcpy(name,newString[1]);
          printf("(%d , %s , %s )\n",roll_number,name,email);
          Student *s=(struct Student*)malloc(sizeof(struct Student));
          EnterStudent(roll_number,name,s,email);
          node* n;
			deletion1(r,&root);
			Sorted_Doubly_Linked_Listdelete_student(r);

			n = Sorted_Doubly_Linked_ListCreate(s);
                        insertion(roll_number,&root,n);






		}

		else if (ch1==8)
		{
			FILE *fptr;
    			if ((fptr = fopen("student_data.txt", "w")) == NULL) 
    			 {
      			printf("Error! opening file");
                    // Program exits if file pointer returns NULL.
                       exit(1);
                         }
                        fprintf(fptr, "%d",mind);
                        fputs("\n",fptr);
			node* t =head;
			//string record = "";
			while(t!=NULL)
			{
				//fputs(t->student->roll_number,fptr);
                                fprintf(fptr,"%d",t->student->roll_number);
                                fputs(" ",fptr);
                                fputs(t->student->name,fptr);
                                fputs(" ",fptr);
                                fputs(t->student->emailID,fptr);
                                fputs("\n",fptr);
				t=t->next;
			}
                   fclose(fptr);
		}
		else
		{
			printf("Wrong Option Selected \n");
		}
	}	
	
	printf("\n");
       
	return 0;
}
